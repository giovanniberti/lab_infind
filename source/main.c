#include "ladder_logic.h"
#include "manage_io.h"
#include "variable.h"

#include "gpio_rpi.h"

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


/* Polling interval in milliseconds expressed in nanoseconds */
#define POLLING_INTERVAL            (1000 * 1000 * 1000)
#define NANOSEC_IN_SEC              (1000 * 1000 * 1000)

#define GPIO_NOTIFY_CYCLE           22


static struct variables variables;


static void signalHandler(int signo);



static inline void sleepUntil(struct timespec *until)
{
    while(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, until, NULL) != 0 && errno == EINTR);
}

int main(void)
{
    /* Local variables */
    struct timespec sleep_until;

    /* Register signal handler function for SIGNTERM and SIGINT */
    if((signal(SIGTERM, signalHandler) == SIG_ERR) || (signal(SIGINT, signalHandler) == SIG_ERR)) {
        printf("Unable to catch signals\n");
        exit(1);
    }

    /* Register GPIO used to notify start of applicative cycle */
    (void)gpioExport(GPIO_NOTIFY_CYCLE);
    (void)gpioDirection(GPIO_NOTIFY_CYCLE, GPIO_DIR_LOW);

    /* Initialize variables */
    initialize_variables(&variables);

    /* Retrieve current time */
    clock_gettime(CLOCK_MONOTONIC, &sleep_until);

    /* PLC loop */
    while(1) {
        /* Notify start of cycle by toggling GPIO */
        (void)gpioToggle(GPIO_NOTIFY_CYCLE);

        /* Acquire system inputs */
        acquire_inputs(&variables);

        /* Manage execution of Ladder Logic */
        manage_ll(&variables);

        /* Produce system outputs */
        produce_outputs(&variables);

        /* Wait until end of cycle */
        sleep_until.tv_nsec += POLLING_INTERVAL;
        if(sleep_until.tv_nsec >= NANOSEC_IN_SEC) {
            ++sleep_until.tv_sec;
            sleep_until.tv_nsec -= NANOSEC_IN_SEC;
        }
        sleepUntil(&sleep_until);
    }

    return 0;
}

static void signalHandler(int signo)
{
    /* Signal handler, check if received signal is a SIGTERM or SIGINT */
    if((signo == SIGTERM) || (signo == SIGINT)) {
        /* Unregister GPIO used to notify start of applicative cycle */
        (void)gpioUnexport(GPIO_NOTIFY_CYCLE);
        /* Release all resources and exit */
        cleanup_variables(&variables);
        exit(0);
    }
}
