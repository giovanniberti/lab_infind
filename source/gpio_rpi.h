/**
 * Library to access GPIO through SYSFS on Raspberry Pi.
 *
 * This library exposes functions used to access GPIO through SYSFS.
 *
 * SYSFS interface for accessing GPIO can be found at:
 *   https://www.kernel.org/doc/Documentation/gpio/sysfs.txt
 *
 * LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * COPYRIGHT
 *
 * Copyright (C) 2017 Marco Papini
 *
 */

#ifndef GPIO_RPI_H_
#define GPIO_RPI_H_


/**
 * @description:    Enumeration of GPIO directions
 */
enum gpio_dir_e {
    GPIO_DIR_IN = 0U,
    GPIO_DIR_OUT,
    GPIO_DIR_LOW,
    GPIO_DIR_HIGH,
    NUM_GPIO_DIRECTIONS
};

/**
 * @description:    Enumeration of GPIO values
 */
enum gpio_value_e {
    GPIO_VALUE_LOW = 0U,
    GPIO_VALUE_HIGH,
    NUM_GPIO_VALUES
};


/**
 * @description:    Export GPIO
 * @param:          pin: GPIO to be exported
 * @return:         0 in case of success, -1 otherwise
 */
int gpioExport(unsigned int pin);

/**
 * @description:    Unexport GPIO
 * @param:          pin: GPIO to be unexported
 * @return:         0 in case of success, -1 otherwise
 */
int gpioUnexport(unsigned int pin);

/**
 * @description:    Set GPIO direction
 * @param:          pin: GPIO over which direction shall be set
 * @param:          dir: GPIO direction to be set
 * @return:         0 in case of success, -1 otherwise
 */
int gpioDirection(unsigned int pin, enum gpio_dir_e dir);

/**
 * @description:    Read GPIO value
 * @param:          pin: GPIO from which value shall be read
 * @return:         0 in case of success, -1 otherwise
 */
int gpioRead(unsigned int pin);

/**
 * @description:    Write GPIO value
 * @param:          pin: GPIO over which value shall be written
 * @param:          value: value that shall be written to GPIO
 * @return:         0 if the GPIO status is logical LOW, 1 if the GPIO
 *                  status is logical HIGH, -1 otherwise (error)
 */
int gpioWrite(unsigned int pin, enum gpio_value_e value);

/**
 * @description:    Toggle GPIO value
 * @param:          pin: GPIO for which value shall be toggled
 * @return:         0 in case of success, -1 otherwise
 */
int gpioToggle(unsigned int pin);


#endif // GPIO_RPI_H_
