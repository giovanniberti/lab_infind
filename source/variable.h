#ifndef VARIABLE_H_
#define VARIABLE_H_


#define MAX_VARS                            100


struct io_var
{
    unsigned int id;
    unsigned int gpio;
    unsigned char value;
};

struct local_var
{
    unsigned int id;
    unsigned char currValue;
    unsigned char nextValue;
};

struct variables
{
    unsigned char numInputs;
    unsigned char numOutputs;
    unsigned char numLocals;
    struct io_var inputs[MAX_VARS];
    struct io_var outputs[MAX_VARS];
    struct local_var locals[MAX_VARS];
};


#endif /* VARIABLE_H_ */
