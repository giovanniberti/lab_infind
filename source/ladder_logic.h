#ifndef LADDER_LOGIC_H_
#define LADDER_LOGIC_H_


#include "variable.h"


void initialize_variables(struct variables *vars);
void cleanup_variables(struct variables *vars);

void manage_ll(struct variables *vars);


#endif /* LADDER_LOGIC_H_ */
