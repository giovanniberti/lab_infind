#ifndef MANAGE_IO_H_
#define MANAGE_IO_H_


#include "variable.h"


void acquire_inputs(struct variables *vars);
void produce_outputs(struct variables *vars);


#endif /* MANAGE_IO_H_ */
