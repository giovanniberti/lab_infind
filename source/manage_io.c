#include "manage_io.h"
#include "gpio_rpi.h"


void acquire_inputs(struct variables *vars) {
    /**
     * For each input:
     * - Read value from GPIO
     */

    for (int i = vars->numInputs; i >= 0; --i) {
        int input_tmp = gpioRead(vars->inputs[i].gpio);

        if(input_tmp == -1) {
            // do nothing
        } else {
            vars->inputs[i].value = (unsigned char) input_tmp;
        }
    }
}

void produce_outputs(struct variables *vars) {
    /**
     * For each output:
     * - Write value to GPIO
     */

    for (int i = vars->numOutputs; i >= 0; --i) {
        unsigned char output = vars->outputs[i].value;
        enum gpio_value_e output_value;
        if (output==1){
            output_value = GPIO_VALUE_HIGH;
        }
        else{
            output_value = GPIO_VALUE_LOW;
        }
        gpioWrite(vars->outputs[i].gpio, output_value);
    }
}
// Non c'è bisogno di considerare il caso  output==-1 perchè gli output sono risultati di operazioni logiche
