#include "ladder_logic.h"

#include "gpio_rpi.h"



void initialize_variables(struct variables *vars)
{
    /* Local variables */
    unsigned int id;

    /* Initialize ID to 0 (first ID) */
    id = 0;

    /* Initialize variables (hard-coded) */
    vars->numInputs = 3;
    vars->inputs[0].id = id++;              /* STOP ID */
    vars->inputs[0].gpio = 10;              /* STOP GPIO */
    vars->inputs[1].id = id++;              /* FORWARD ID */
    vars->inputs[1].gpio = 9;               /* FORWARD GPIO */
    vars->inputs[2].id = id++;              /* REVERSE ID */
    vars->inputs[2].gpio = 11;              /* REVERSE GPIO */

    vars->numOutputs = 3;
    vars->outputs[0].id = id++;             /* ALARM ID */
    vars->outputs[0].gpio = 13;             /* ALARM GPIO */
    vars->outputs[1].id = id++;             /* FWD ID */
    vars->outputs[1].gpio = 19;             /* FWD GPIO */
    vars->outputs[2].id = id++;             /* REV ID */
    vars->outputs[2].gpio = 26;             /* REV GPIO */

    vars->numLocals = 3;
    vars->locals[0].id = id++;              /* M1 */
    vars->locals[1].id = id++;              /* M2 */
    vars->locals[2].id = id++;              /* PROT */

    /**
     * For each input and output:
     * - Export GPIO
     * - Set direction (IN for inputs, LOW for outputs)
     */

    for (int i = vars->numInputs - 1; i >= 0; --i) {
        gpioExport(vars->inputs[i].gpio);
        gpioDirection(vars->inputs[i].gpio, GPIO_DIR_IN);
    }

    for (int i = vars->numOutputs - 1; i >= 0; --i) {
        gpioExport(vars->outputs[i].gpio);
        gpioDirection(vars->outputs[i].gpio, GPIO_DIR_LOW);

    }
}

void cleanup_variables(struct variables *vars)
{
    /* Local variables */
    int idx;

    /**
     * For each input and output:
     * - Unexport GPIO
     */
    for(idx = (vars->numInputs - 1); idx >= 0; --idx) {
        (void)gpioUnexport(vars->inputs[idx].gpio);
    }

    for(idx = (vars->numOutputs - 1); idx >= 0; --idx) {
        (void)gpioUnexport(vars->outputs[idx].gpio);
    }
}

void manage_ll(struct variables *vars)
{
    /* Local variables */
    int idx;

    /**
     * Ladder logic equations:
     * - M1 = not(STOP) and not(PROT) and (FORWARD or M1) and not(M2)
     * - M2 = not(STOP) and not(PROT) and (REVERSE or M2) and not(M1)
     * - PROT = not(STOP) and (PROT or (M1 and M2))
     * - ALARM = PROT
     * - FWD = M1 and not(M2)
     * - vars->outputs[2].value = M2 and not(M1)
     */

    /*
    vars->inputs[0].id = id++;               STOP ID
    vars->inputs[0].gpio = 10;               STOP GPIO 
    vars->inputs[1].id = id++;               FORWARD ID 
    vars->inputs[1].gpio = 9;                FORWARD GPIO 
    vars->inputs[2].id = id++;               REVERSE ID 
    vars->inputs[2].gpio = 11;               REVERSE GPIO 

    vars->numOutputs = 3;
    vars->outputs[0].id = id++;              ALARM ID 
    vars->outputs[0].gpio = 13;              ALARM GPIO 
    vars->outputs[1].id = id++;              FWD ID 
    vars->outputs[1].gpio = 19;              FWD GPIO 
    vars->outputs[2].id = id++;              REV ID 
    vars->outputs[2].gpio = 26;              REV GPIO 

    vars->numLocals = 3;
    vars->locals[0].id = id++;               M1 
    vars->locals[1].id = id++;               M2 
    vars->locals[2].id = id++;               PROT 
     */

    /*
     * Ladder logic equations:
     * - M1 = not(STOP) and not(PROT) and (FORWARD or M1) and not(M2)
     * - M2 = not(STOP) and not(PROT) and (REVERSE or M2) and not(M1)
     * - PROT = not(STOP) and (PROT or (M1 and M2))
     * - ALARM = PROT
     * - FWD = M1 and not(M2)
     * - REV = M2 and not(M1)
     */


    unsigned char curr_m1 = (vars->inputs[0].value);
    unsigned char curr_m2 = vars->inputs[1].value;
    unsigned char curr_prot = vars->inputs[2].value;

    unsigned char curr_stop = vars->outputs[0].value;
    unsigned char curr_forward = vars->outputs[1].value;
    unsigned char curr_reverse = vars->outputs[2].value;


    vars->locals[0].nextValue = !curr_stop & !curr_prot & (curr_forward | curr_m1) & !curr_m2; // M1
    vars->locals[1].nextValue = !curr_stop & !curr_prot & (curr_reverse | curr_m2) & !curr_m1; // M2
    vars->locals[2].nextValue = !curr_stop & (curr_prot | (curr_m1 & curr_m2)); // PROT

    vars->outputs[0].value = vars->locals[2].nextValue; // ALARM
    vars->outputs[1].value = vars->locals[0].nextValue & !(vars->locals[1].nextValue); // FWD
    vars->outputs[2].value = vars->locals[1].nextValue & !(vars->locals[2].nextValue); // REV
    
    /* For each Local variable, update its Current State using the Next State */
    for(idx = (vars->numLocals - 1); idx >= 0; --idx) {
        vars->locals[idx].currValue = vars->locals[idx].nextValue;
    }
}
